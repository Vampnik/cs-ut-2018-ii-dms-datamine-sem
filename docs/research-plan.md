# Research plan
### Understanding team performance in agile software development
##### Tõnis Ojandu
##### Supervisor: Mario Ezequiel Scott

## Introduction

Scrum is an agile workflow management framework that is often used for software development. It breaks the overall
workflow into small timeboxed iterations, called sprints. These sprints that usually last from 2 weeks to 4 weeks are 
planned by the team together. This involves breaking agreed-upon jobs into stories that can be later assigned to team 
members to be solved individually during the spring. At the the end of the sprint retrospective meeting is held. 
Gathered experience from previous sprint(s) along with new details from outside the team are used to plan the next
sprint.

The idea behind breaking the job into story points is to ease the time estimation process. Since the sprints are
designed to be short then the planning will require less detailed software analysis. Also as the team adjusts to working
together the work load and time-estimation per single story point should even out through time. As this happens team
and individual performance metrics can be calculated that will help with managerial operations and more rapid response
to performance issues. This metrics among others can include:

* Story points that the team or individual solves during sprint, called **velocity**. This helps with consecutive sprint 
plannings. 
* **Burndown chart/curve** that maps the the team or individual story point solved on a time series chart and helps 
determining unevennesses in story point plannings.  

The goal of this research project is to use association rules mining to get a better understanding of which 
characteristics of the Scrum stories are frequently related to the individual performance of the team members. Thus this 
research project is more exploratory in nature and aims to lay ground work for future further studies.

## Data collection

Dataset that will be used for this research project contains the Jira issue reports from 8 different open source projects. 
The open source projects included in the dataset are:

* Aptana Studio (APSTUD), a web development IDE;
* Dnn Platform (DNN), a web content management system;
* Apache Mesos (MESOS), a cluster manager;
* Mule (MULE), a lightweight Java based enterprise service bus and integration platform;
* Sonatype’s Nexus (NEXUS), a repository manager for software artifacts required for development;
* Titanium SDK/CLI (TIMOB), an SDK and a Node.js based command-line tool for managing, building, and deploying Titanium projects;
* Appcelerator Studio (TISTUD), an Integrated Development Environment (IDE);
* Spring XD (XD), a unified, distributed, and extensible system for data ingestion, real-time analytics, batch processing, and data export.

The dataset consists of three files:

* A CSV file containing contains data about the issue reports of all the projects. This file contains tuple-s like:
    * fields.assignee.name = 
    * fields.components = [{u'id': u'12786', u'self': u'https://jira.spring.io/rest/api/2/component/12786', u'description': u'Server and Module property configuration', u'name': u'Configuration'}]
    * fields.created = 2016-03-31T22:35:55.000+0000
    * fields.creator.name = john.doe
    * fields.description = The MapReduce samples should have "yarn.resourcemanager.scheduler.address" since it might be needed in a multi node production cluster.
    * fields.fixVersions = []
    * fields.issuetype.name = Improvement
    * fields.issuetype.subtask = False
    * fields.priority.name = Minor
    * fields.reporter.name = john.doe
    * fields.resolution.description = 
    * fields.resolution.name = 
    * fields.resolutiondate = 
    * fields.status.id = 10000
    * fields.status.name = To Do
    * fields.status.statusCategory.name = To Do
    * fields.summary = Add "yarn.resourcemanager.scheduler.address" to mapreduce samples
    * fields.updated = 2016-03-31T22:35:55.000+0000
    * fields.versions = []
    * fields.watches.watchCount = 1
    * key = XD-3753
    * storypoints =	2.0
    * project =	xd
    * sprint =	
* A Changelog CSV file that contains data about the changes made to the issue reports of all the projects.
* A CSV file that contains data about the users (assignees, reporters, creators).

Also as the dataset in raw form then it needs to be cleaned before associations can be mined. This may involve sampling
and removing outlier data points, such as:

* all issues may not be Scrum stories,
* some issue owners/assignees may not be scrum team members,
* not all sprints designated as such may be Scrum sprints,
* and all other unforeseeable obstacles that may need to be overcome.

## Proposed Approach

From the data features for issues are selected. These can include priority of the issue, amount of overall 
detail/information in the issue, time spent on the issue etc. These features values, if not already, will be mapped into
finite discrete values. For example time on issue might be mapped to LITTLE_TIME, MEDIUM_TIME, A_LOT_OF_TIME etc. For 
each specific feature values the better mapping may be linear, exponential etc. This needs to be explored in this and
is also bulk of this research. As a result this approach will generate a issue feature matrix. For example:

| Issue  | PRIO_HIGH | PRIO_MED | PRIO_LOW | TIME_LITTLE | TIME_MED | TIME_ALOT | ...
|--------|-----------|----------|----------|-------------|----------|-----------|-----
| XD-123 | 1         | 0        | 0        | 0           | 1        | 0         | ...
| XD-321 | 0         | 0        | 1        | 0           | 0        | 1         | ...
| ...    | ...       | ...      | ...      | ...         | ...      | ...       | ...

For this research each issue will be directly mapped to a single team member. For each issue the team member that spent 
the most time being assigned to it will be mapped to the issue. Using all the issues that are mapped to the team members 
Scrum velocity and individual performance from the burndown chart will be calculated for each sprint. Since there are 
only one sprint and one team member mapped to any issue then these metrics can be included to the previous matrix. 
In order to include them in the matrix these continuous values will also have to be mapped into discrete finite values.
Resulting matrix might look like:

| Issue  | PRIO_HIGH | ... | VELOCITY_HIGH | VELOCITY_MED | VELOCITY_LOW | ... 
|--------|-----------|-----|---------------|--------------|--------------|-----
| XD-123 | 1         | ....| 1             | 0            | 0            | ...
| XD-321 | 0         | ... | 0             | 1            | 0            | ...
| ...    | ...       | ... | ...           | ...          | ...          | ...

This matrix will be used to machine learn association rules. For given features item set I={i_1, i_2, ..., i_3} 
contains all the available discrete mappings for the feature. Association rules are defined as X => Y where X and Y are
subsets of I. Resulting association rules may be:

* (PRIO_HIGH, TIME_LITTLE) -> (VELOCITY_HIGH)
* (DESCRIPTION_LITTLE) -> (PERFORMANCE_LOW)
* (DESCRIPTION_LITTLE) -> (TIME_ALOT)
* etc

Due this end association rule learning algorithm **Apriori Algorithm** could be used.

It should be noted that these rules will be calculated per project. Mixing all the projects is not be sensible since 
team members and Scrum implementation usually vary from project to project.

## Interpretation and Evaluation

In order to evaluate the strength of the association rule, its support confidence and lift can be used as the metrics.
Proposed Apriori Algorithm also uses them for evaluation.

All the subsets of the item set are called transactions. Given a transactions A and B:

* support for B is calculated: Support(B) = (Transactions containing (B))/(Total Transactions)
* confidence for A -> B is calculated: Confidence(A -> B) = (Transactions containing both (A and B))/(Transactions containing A)
* lift for A -> B is calculated: Lift(A -> B) = (Confidence (A -> B))/(Support (B)) 

Association strength is defined as lift for this research purposes. 

Since the focus of the research is to determine associations between story features and team (and team member)
productivity, then machine-learned rules that to not pertain to this will be filtered/excluded from the end result.
This means that only association rules containing Scrum performance metric value items will be included and interpreted.

Success of this research relies on successful identification of meaningful feature mappings and interpretation of the 
resulting association rules. Failure of the research may also occur if meaningful features cannot be obtained from the 
given dataset and/or Scrum sprint details in the dataset are incorrect, which will make calculating Scrum metrics
impossible.

## Research plan

| Date/Period                | Type         | Description
|----------------------------|--------------|------------------
| **13 of September 14:15**  | **Deadline** | **Kick-off seminar**
| **20 of September 14:00**  | **Deadline** | **Topic confirmation**
| **27 of September 12:00**  | **Deadline** | **Deadline for the research plan**
| ~3 weeks                   | Task         | Cleaning the data. Choosing the features. Implementing research frame.
| ~2 weeks                   | Task         | Datamining association rules. Interpreting the results.
| ~2 weeks                   | Task         | Writing.
| **18th of November 23:59** | **Deadline** | **Deadline for the first draft to be submitted for reviewing**
| ~2 weeks                   | Task         | Reviewing. Minor clean up.
| **2nd of December 23:59**  | **Deadline** | **Deadline for reviews**
| ~1 week                    | Task         | Final clean up.
| **13th of January 23:59**  | **Deadline** | **Final version of the project report**
| **?**                      | **Deadline** | **Defending the project**

## References

[1] Downey, Scott & Sutherland, Jeff. (2013). Scrum Metrics for Hyperproductive Teams: How They Fly like Fighter 
Aircraft. 4870-4878. 10.1109/HICSS.2013.471. 

[2] Scott E., Pfahl D. (2017) Exploring the Individual Project Progress of Scrum Software Developers. In: Felderer M., 
Méndez Fernández D., Turhan B., Kalinowski M., Sarro F., Winkler D. (eds) Product-Focused Software Process Improvement. 
PROFES 2017. Lecture Notes in Computer Science, vol 10611. Springer, Cham

[3] The Art and Science of Analyzing Software Data, Chapter 11.3.2 Gustavo Ansaldi Oliva, Marco Aurélio Gerosa
